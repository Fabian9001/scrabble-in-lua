
-- function prints entire content of some table to console (or to a file if 'print' is adjusted accordingly)
-- depth-first
-- won't get stuck in a loop if graph is circular
-- function( table, [,function] [,table] ) -> nil

return --[[ table.print = ]] function( root, print, listed )

  -- get default setting if needed
  print = print or _G.print --[[ argument 'print' must have signature:
    function(string) -> nil, e.g. function(str)file:write(str.."\n")end ]]
  listed = listed or {} --[[ items in 'listed' are expected to have  
    table addresses as keys. And, values should be strings or booleans.
    (However, no error if that's not the case.) ]]
  
  -- basic input checks
  assert( type(root)=="table",
    "\n\n\tbad argument #1 to 'root' (table expected, got "
    .. type(root) .. ")\n" )
  assert( type(print)=="function",
    "\n\n\tbad argument #2 to 'print' (function expected, got "
    .. type(print) .. ")\n" )
  assert( type(listed)=="table",
    "\n\n\tbad argument #3 to 'listed' (table expected, got "
    .. type(listed) .. ")\n" )

  -- main loop
  local function recursion( tbl, keypath, indent )
  
    -- keep track of what's already been searched to prevent getting stuck in a loop
    listed[tbl] = keypath 
    
    -- search current table
    for k, v in pairs(tbl) do
      if type(k)=="string" then k='.'..k else k='['..tostring(k)..']' end
      if type(v)=="table" then
        if listed[v] then
          print( string.rep("|   ",indent) .. k
            .. " (already listed: "
            .. tostring(listed[v]) .. ")" )--tostring for savety
        else
          print( string.rep("|   ",indent) .. k )
          recursion( v, keypath .. k, indent+1 ) -- go deeper into the tree
        end
      else
        print( string.rep("|   ",indent) .. k
          .. " = "
          .. tostring(v) )
      end
    end
  end
   
  -- initial call to recursion to start printing
  print( "printing '".. tostring(root) .. "':" )
  recursion( root, "", 1 )
end

-- dofile("C:/Users/Werner/VirtualBox VMs/Debian/shared/tableprint.lua")