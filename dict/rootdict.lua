-- tree class

return {

  Print = function( node, printer--[[nil]], listed--[[nil]], value_meets_condition_to_be_printed--[[nil]] )
  
    -- user setup or default setup
    printer = printer or _G.print--for example: function(str)file:write(str.."\n")end (file would need to be handled appropriately before and after call to 'print')
    listed = listed or {}
    value_meets_condition_to_be_printed = value_meets_condition_to_be_printed or function(v,k)return true end
    
    -- basic input checks
    assert( type(node)=="table",
      "\n\n\tbad argument #1 to 'node' (table expected, got "
      .. type(node) .. ")\n" )
    assert( type(printer)=="function",
      "\n\n\tbad argument #2 to 'printer' (function expected, got "
      .. type(printer) .. ")\n" )
    assert( type(listed)=="table",
      "\n\n\tbad argument #3 to 'listed' (table expected, got "
      .. type(listed) .. ")\n" )
    assert( type(value_meets_condition_to_be_printed)=="function",
      "\n\n\tbad argument #4 to 'value_meets_condition_to_be_printed' (function expected, got "
      .. type(value_meets_condition_to_be_printed) .. ")\n" )
  
    -- main loop
    local function recursion( tbl, keypath, indent )
    
      -- keep track of what's already been searched to prevent getting stuck in a loop
      listed[tbl] = keypath 
      
      -- search current table
      for k, v in pairs(tbl) do
        if type(k)=="string" then k='.'..k else k='['..tostring(k)..']' end
        if type(v)=="table" then
          if not(listed[v]) then -- __index == tbl
            printer( string.rep("|   ",indent) .. k )
            recursion( v, keypath .. k, indent+1 ) -- go deeper into the tree
          end
        elseif value_meets_condition_to_be_printed(v,k) then
          printer( string.rep("|   ",indent) .. k
            .. " = "
            .. tostring(v) )
        end
      end
    end
     
    -- initial call to recursion to start printing
    printer( "printing ".. tostring(node) .. " where .word = "
      .. ( rawget(node,"word") or ("(nil) and parent.word = " .. (node.word or "")) )
      .. "\n." .. node:getmyname() )
    recursion( node, "", 1 )
  end,

  sprint_ = function( node, str )-- search + print_ = sprint | e.g., node:print__("katze")
    node = node:search( str )
    print( str .. " --> children: " .. node:getchildstr(" ") )
    node:print_()
  end,

  print_ = function( node )-- node:print_()
    node:Print( print, {},
      function(v)return not(type(v)=="function")end )-- hide methods when printing tree
  end,

  print = function( node )-- node:print()
    for k, v in pairs( node ) do print(k, v) end
  end,
  
  newnode = function( parentnode, char )
    parentnode.__index = parentnode--enable parent to pass on methods
    parentnode[char] = setmetatable({}, parentnode)--new node inherits methods from parent
  end,

  getmyname = function( node )--| f(table) -> str
    local parent = getmetatable(node) or {}
    for name, childnode in pairs(parent) do
      if childnode==node then return name end
    end
    return ""
  end,

  getchildstr = function( node, delimiter )--| f(table,str) -> str
    delimiter = delimiter or ""
    local str = ""
    for k, _ in pairs(node) do
      if k:len()==1 then
        str = str .. k .. delimiter
      end
    end
    return str
  end,

  -- idempotent operation
  insert = function( node, wordextension )--| f(table,str) -> table
    local wordbase = node.word or ""
    for char in wordextension:gmatch(".") do
      if not(rawget(node,char)) then node:newnode(char) end
      node = node[char]-- move to the next node, i.e. next char in wordextension
    end
    node.word = wordbase .. wordextension
    return node
  end,
  
  search = function( node, word )--| f(table,str) -> table
    for char in word:gmatch(".") do
      if not(rawget(node,char)) then -- can't simply do node[char]==nil because inheritance
        node.__index = node-- return dummy:
        return setmetatable({}, node)
      end
      node = node[char]-- move to the next node
    end
    return node
  end,

  status_ = function( node, str )--| f(table,str) -> str
    local node, exis, expa = node:status(str)
    local exists = { [false]=" doesn't", [true]=" does" }
    local isexpandable = { [false]=" isn't", [true]=" is" }
    local evalxor = { [false]=" and", [true]=" but" }
    return "'" .. str .. "'" .. exists[exis] .. " exist" ..
      evalxor[(exis and not(expa)) or (expa and not(exis))] ..
      isexpandable[expa] .. " expandable into something that exists ( " ..
      node:getchildstr(" ") .. ")"
  end,

  status = function( node, str )--| f(table,str) -> table, boolean, boolean
    node = node:search(str)
    for k, v in pairs( node ) do
      if k:len()==1 then
        return node, rawget(node, "word") ~= nil, true
      end
    end
    return node, rawget(node, "word") ~= nil, false
  end,

  isexpandable = function( node, str )--| f(table,str) -> boolean
    for k, _ in pairs( node:search(str) ) do
      if k:len()==1 then return true end--assuming there is no method with string length 1
    end
    return false
  end,

  isword = function( node, str )--| f(table,str) -> boolean
    return rawget(node:search(str), "word") ~= nil
  end,
  
  init = function( root, list, normalize )
    for i=1, #list do
      root:insert( normalize(list[i].title) )
    end
    root.init = nil
    return root
  end
  
}
