return function( self, normalize )
  
  local root = {}
  function( parentnode )
    local newnode = setmetatable( {children={},isendofword=false}, parentnode )
    return 
  end
  
  local function insert( rootnode, word )
    local curnode = rootnode-- current node
    for char in word:gmatch("%w") do
      if not( char:is_child(curnode) ) then
        curnode.children[char] = node( curnode )
      end-- Move to the next node:
      curnode = curnode.children[char]
    end-- Mark the end of the word:
    curnode.isendofword = true
  end
  
  local function init( self, list )
    for i=1, #list do
      insert( self.root, normalize(list[i].title) )
    end  
  end
  
  local function getchildrenstring( node )--| f(node) -> str
    local str = ""
    for k in pairs(node.children) do str=str.." "..k end
    return str
  end

  string.is_child = function( char, node )
    for key in pairs(node.children) do
      if char == key then return true end
    end
    return false
  end
  
  print("Initializing tree structure...")
  init( self, self.indexlist )
  print("... done.")
  
  return function( self, word )--search function
    local curnode = self.root-- current node
    for char in word:gmatch("%w") do
      if not( char:is_child(curnode) ) then
        return false
      end-- Move to the next node:
      curnode = curnode.children[char]
    end-- Mark the end of the word:
    return curnode.isendofword
  end
end
