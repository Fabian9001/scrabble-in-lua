
-- create hashlist (Due to normalizing, some words will occur twice (e.g. "Sein" and "sein").
-- So, we combine them in 1 table. After having been normalized, about 1% of all words occur twice.)

return {
  init = function( hashlist, indexlist, normalize )
    
    -- main loop
    local index = 1
    while (index < #indexlist) do
    
      local page = indexlist[index] -- readability++
      local normalizedTitle = normalize(page.title) -- normalize title
      local hash = hashlist[normalizedTitle] -- readability
  
      if hash==nil then
      
        -- create reference to page in hashlist
        hashlist[normalizedTitle] = setmetatable(page, table)
        
        -- hashlist entry is not ambiguous at this point
        hashlist[normalizedTitle].ambiguous = 0
        
        -- go to next page
        index = index + 1
      else
        
        -- mark page as ambiguous by incrementing its ambiguous count
        hash.ambiguous = hash.ambiguous + 1
        
        -- insert additional Bedeutung from title 
        table.insert( hash.bedeutung, page.title .. " (" .. page.wortart .. "): " .. (page.bedeutung[1] or "") )
        table.insert( hash.bedeutung, page.title .. " (" .. page.wortart .. "): " .. (page.bedeutung[2] or "") )
        
        -- overwrite current page with very last page in list, then delete reference to last page
        indexlist[index] = indexlist[#indexlist]
        indexlist[#indexlist] = nil
        
        -- do NOT go to next page ( lol ! )
        index = index
      end
    end

    hashlist.init = nil -- init once
    return hashlist
  end
}

