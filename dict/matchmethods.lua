
return function()
  return
-- createMatchList
  function( self, nPattern, maxWordLength, matchlimit--[[math.maxinteger]] )
    do local _, len = nPattern:gsub("%[[^%]]+%]",""):gsub("%w","") -- use :gsub to count alpha-numeric chars in pattern (and not stuff like ^ + . $ ? ] [)
      maxWordLength = maxWordLength or (len + 8) end-- default maxWordLength is length of set + chars available on board
    matchlimit = matchlimit or math.maxinteger
  
    print( "pattern = '" .. nPattern ..
      "'\tmaxWordLength = " .. maxWordLength ..
      "\tlimit #results = " .. matchlimit )
  
    local matches = setmetatable( {}, table )
    for nWord, page in pairs(self.hashlist) do
      if #matches < matchlimit
        and string.match(nWord, nPattern)
          and string.len(nWord) <= maxWordLength then 
            matches:insert( page )
      end
    end
  
    return matches
  end,
  
-- printMatchList = createMatchList + printing
  function( self, nPattern, maxWordLength, matchlimit, printLimit )
    printLimit = printLimit or 19
    local count, matches = 0, self:createMatchList(nPattern, maxWordLength, matchlimit--[[may be nil]])
    for i=1, #matches do 
      count=count+1
      if count > printLimit then break end
      print( i, matches[i].title, matches[i].bedeutung[1] )   
    end
    print( "print limit:", printLimit, "#matches:", #matches )
    return matches
  end,
  
-- match f(dict, str, int|nil, int|nil) -> str
  function( self, nPattern, maxWordLength, matchlimit )
    local str, count = "", 0
    for k, page in ipairs(self:createMatchList(
      nPattern, maxWordLength, matchlimit or 49)) do
        count = count + 1
        str = str .. " " .. page.title
        if count%7 == 0 then str = str .. "\n" end
    end
    return str .. "\n#results = " .. count
  end
end

--function dict:patfits( pattern, r, c )
--  patlen = pattern:gsub("[^%w%.]",""):len()
--  return patlen + c - 1 <= 15, patlen + r - 1 <= 15  
--end
