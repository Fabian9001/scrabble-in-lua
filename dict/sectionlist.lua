-- sorted dictionary is prerequisite to get sectionlist
return function( indexlist, normalize )

  -- sort dictionary
  print("Sorting ...")
  indexlist:sort( function(a,b) return normalize(a.title) < normalize(b.title) end )
  print("... done sorting.")

  -- table to hold section info
  local sectionlist = setmetatable( {}, table )

  -- transforms uppercase to lowercase, and, special german chars to 1-digit integer
  local normalizeCodepointToASCII = setmetatable({ 
    ['A']='a',
    ['B']='b',
    ['C']='c',
    ['D']='d',
    ['E']='e',
    ['F']='f',
    ['G']='g',
    ['H']='h',
    ['I']='i',
    ['J']='j',
    ['K']='k',
    ['L']='l',
    ['M']='m',
    ['N']='n',
    ['O']='o',
    ['P']='p',
    ['Q']='q',
    ['R']='r',
    ['S']='s',
    ['T']='t',
    ['U']='u',
    ['V']='v',
    ['W']='w',
    ['X']='x',
    ['Y']='y',
    ['Z']='z',
    ---------
    ['Ä']='1',
    ['Ö']='2',
    ['Ü']='3',
    ['ẞ']='4',
    ['ä']='1',
    ['ö']='2',
    ['ü']='3',
    ['ß']='4' }, { __index=function(tbl,key) return key end })
  
  -- find first word that start with 'a', 'b', ..., 'z'
  for _, v in pairs( normalizeCodepointToASCII ) do
    local pat = '^' .. v
    local i = 1
    while indexlist[i] do
      local title = normalize(indexlist[i].title)
      if title:match(pat) then
        print(i, title, v)
        sectionlist[v] = i-- save index of word
        break
      end
      i=i+1
    end
  end

  -- return sectionlist
  return sectionlist
end 

--[[
917     a2de    a
10223   b1bbeln b
46141   iahen   i
48260   j1ckchen        j
34724   g1a     g
40493   h14     h
23792   eagle   e
29135   f1cheln f
18261   c1cuber c
19520   d11     d
182     22mrang 2
353     3bel    3
182     22mrang 2
1       11mol   1
353     3bel    3
1       11mol   1
76389   qanat   q
76845   r14     r
69057   oachkatzlschwoaf        o
70680   p1chter p
60548   m14ig   m
66127   n1chst  n
49238   k1fer   k
56798   l1cheln l
109815  y1rb    y
1366    zytozym z
105297  w1chsern        w
109701  xantener        x
98351   ube     u
100850  v1terkarenz     v
81063   s1bel   s
94016   t1feln  t
]]