return function( cleanupdictionary )
--dict.remove:
  return
    function( self ) --| f() -> 
      for word, index in pairs(self.words_to_be_removed) do
        self.indexlist[index] = self.indexlist[#self.indexlist]
        self.hashlist[word] = nil
        self.words_to_be_removed[word] = nil
        print( "removed", self.indexlist:remove(index).title )
      end
    end,
--dict.tag_as_to_be_removed:
    function( self, nWord, i )--| f(str, int) -> 
      if self.hashlist[nWord] then
  	    self.words_to_be_removed[nWord] = i-- save changes to file for them to be loaded when initializing a new game:
        local file = io.open(cleanupdictionary,"a+")
        assert(file,"\n\n\tno file :(\n\t" .. tostring(cleanupdictionary))
        file:write(i .." : ".. self.indexlist[i].title .."\n")
        file:close()
      else
        print("unknown word '".. tostring(nWord) ..
          "' cannot be removed (press enter to continue)")
        io.read()
      end
    end, 
--dict.words_to_be_removed:
    {}
end

-- elseif user:match("^delete") then
--   self:tag_as_to_be_removed( user:match("%s+(%w+)"), i )
--   self:undo()
--   return false