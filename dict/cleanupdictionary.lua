--dofile("dict/cleanupdictionary.lua")( dict.indexlist, dict.hashlist, normalize, cleanupdictionary )-- singleton
-- singleton
return function( ilist, hlist, normalize, cleanupdictionary ) 
  local file = io.open(cleanupdictionary,"r"); assert(file,"\n\n\tno file\n")
  local line = file:read("l")

  while line do
    local index = tonumber(line:match("^%d+"))--separate number from title
    local title = ilist[index].title
    hlist[normalize(title)] = nil
    ilist[index] = ilist[#ilist]-- overwrite with pointer to last entry in list
    ilist:remove(#ilist)--remove last entry which now is at ilist[index]
    print( "removed:", title )
    line = file:read("l")
  end

  print("file closed:",file:close())
end
