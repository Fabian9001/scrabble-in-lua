-- dict:rm()
return function( self, nWord )
  local word = self.hashlist[nWord]
  if word then
    for index, page in ipairs(self.indexlist) do
      if page==word then
        self.hashlist[nWord] = nil
        self.indexlist[index] = self.indexlist[#self.indexlist]
        self.indexlist:remove(#self.indexlist)
        return true, word
      end
    end
  else
    return false, nil
  end
end