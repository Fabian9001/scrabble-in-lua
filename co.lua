-- ("cls"):ex(); S:replaceall(); cotest(S:getstring(), D.root); S:print()
return function( source, root )

  _G.table.print  = dofile("tableprint.lua"); table.__index=table
  _G.string.print = function(str) print(str) end
  local rootsearch = root.search 
  local n = 0
  root.search = function( node, word )--| f(table,str) -> table
    for char in word:gmatch(".") do
      n = n + 1
      if not( rawget(node,char) ) then -- can't simply do node[char]==nil because inheritance
        node.__index = node-- return dummy:
        return setmetatable( {}, node )
      end
      node = node[char]-- move to the next node
    end
    return node
  end
  
  local permutations = dofile("set/permutation.lua")
  local str = ""
  for char in source:gmatch(".") do
    str = str .. char
    local m  = 0
    local co = coroutine.wrap( permutations )
    local p, running = co( str, nil, nil, coroutine.yield )-- first permutation
    while running do
      if root:exists(p) then
        print("\tmatch:", p)
        m=m+1
        io.read()
      end    
      p, running = co()-- next permutation
    end
    p:print()
    print( "\ttotal n = ".. n, m, str ); io.read()
  end

  root.search = rootsearch
end
