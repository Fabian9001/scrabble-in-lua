local function foo( alreadymatched, char, word, n ) print(n, char)
  local notmatched = ""
  for char2 in word:gmatch(".", n) do print( "", char, char2, n )
    if char==char2 and not(alreadymatched[n]) then
      alreadymatched[n] = true
      return n+1, notmatched
    else
      n = n + 1 
      notmatched = notmatched .. char2--not matched
    end
  end
  return n, notmatched
end

return function( word, pattern )--| f(str, str) -> list
  local str, notmatched, n = "", "", 1
  local alreadymatched = setmetatable( {}, table )
  for char in pattern:gsub("[^%w]",""):gmatch(".") do
    n, notmatched = foo(alreadymatched, char, word, n)
    str = str .. notmatched
  end
  alreadymatched.str = str .. word:sub(n,-1)
  return alreadymatched
end

--[[

function foo( word, pat )
  for k, fset in ipairs(word:gfind(pat)) do
    local a, b = fset.a, fset.b
    print(k, "\n".. word, a, b, word:notmatch(pat, a, b))
  end
end

function foo(word,pat)
  local fset = word:gfind(pat)
  for k, set in ipairs(fset) do
    local a, b = set.a, set.b
    print(k, word:notmatch(pat,a,b))
  end
end

]]