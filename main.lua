
-- signal to user that script is loading
print("Loading Scrabble ...")
math.randomseed( os.time() )

local function get_clear_screen_string()--singleton
  if package.config:sub(1,1)=="\\" then -- Windows
    os.execute("chcp 65001") -- for Windows to display Umlaut correctly
    return "cls" -- Windows
  else
    return "clear"
  end
end

-- load modules
B, S, D, G = dofile( "game/init.lua" )(
    15,
    get_clear_screen_string(),
    "dict/cleanupdictionary.txt" )

-- print board, fill character set
B:print()
S:addrandom( 8 )

-- print some basic info to console
print("\n... done loading Scrabble :)\n")
print("Global variables you can interact with:")
print("\tB, S, D, G")
print("To look into board methods do:")
print("\tB:tprint()")
print("To look into set methods do:")
print("\tS:tprint()")
print("And to look into dictionary methods do:")
print("\tD:tprint()")