
local function normalize(str)
  local normalizeCodepointToASCII = setmetatable({ -- transforms uppercase to lowercase, and, special german chars to 1-digit integer
    ['A']='a',
    ['B']='b',
    ['C']='c',
    ['D']='d',
    ['E']='e',
    ['F']='f',
    ['G']='g',
    ['H']='h',
    ['I']='i',
    ['J']='j',
    ['K']='k',
    ['L']='l',
    ['M']='m',
    ['N']='n',
    ['O']='o',
    ['P']='p',
    ['Q']='q',
    ['R']='r',
    ['S']='s',
    ['T']='t',
    ['U']='u',
    ['V']='v',
    ['W']='w',
    ['X']='x',
    ['Y']='y',
    ['Z']='z',
    ---------
    ['Ä']='1',
    ['Ö']='2',
    ['Ü']='3',
    ['ẞ']='4',
    ['ä']='1',
    ['ö']='2',
    ['ü']='3',
    ['ß']='4'}, { __index=function(tbl,key) return key end })
  local newString = ""
  for char in str:gmatch(utf8.charpattern) do
    newString = newString
      .. normalizeCodepointToASCII[char]
  end
  return newString
end

local function de_normalize(nWord)
  local getReadableChar = setmetatable({ -- to make for readable words, normalized special chars (ä,ö,ü,ß) need to be transformed back. All chars are transformed to uppercase.
    ['a']='A',
    ['b']='B',
    ['c']='C',
    ['d']='D',
    ['e']='E',
    ['f']='F',
    ['g']='G',
    ['h']='H',
    ['i']='I',
    ['j']='J',
    ['k']='K',
    ['l']='L',
    ['m']='M',
    ['n']='N',
    ['o']='O',
    ['p']='P',
    ['q']='Q',
    ['r']='R',
    ['s']='S',
    ['t']='T',
    ['u']='U',
    ['v']='V',
    ['w']='W',
    ['x']='X',
    ['y']='Y',
    ['z']='Z',
    ---------
    ['1']='Ä',
    ['2']='Ö',
    ['3']='Ü',-- any other char remains unchanged:
    ['4']='ẞ'},{__index=function(tbl,key) return key end})
  local word = ""
  for char in nWord:gmatch(utf8.charpattern) do word = word .. getReadableChar[char] end
  return word
end

return function( number_of_rows_and_columns, clearconsole, cleanupdictionary )
  
   -- load some useful global functions
  _G.string.gfind = dofile("gfind.lua") do -- no need to enable inheritance, any string has access to string class by default
  _G.string.x     = function(str)os.execute(str)end
  _G.string.print = function(str,...)print(str,...)end
  _G.table.print  = dofile("tableprint.lua"); table.__index=table--enable inheritance
  _G.table.len = function(self) local n=0; for _,__ in pairs(self) do n=n+1 end return n end
end

  -- dictionary module
  local dict = {} do
  dict.indexlist = setmetatable( dofile("dict/indexlist.lua"), table )
  dict.indexlist.__index  = dict.indexlist
  dict.print = dofile("dict/print.lua")
  dict.hashlist = dofile("dict/hashlist.lua"):init( dict.indexlist, normalize )-- singleton
  dict.root = dofile("dict/rootdict.lua"):init( dict.indexlist, normalize )
  dict.tprint = dofile("dict/tprint.lua")
  --dofile("dict/cleanupdictionary.lua")( dict.indexlist, dict.hashlist, normalize, cleanupdictionary )-- singleton
  --dict.sectionlist = dofile("dict/sectionlist.lua")( dict.indexlist, normalize )-- singleton, sorting
  --dict.rm = dofile("dict/rm.lua")
  dict.createMatchList, dict.printMatchList, dict.match = dofile("dict/matchmethods.lua")()
end

  -- charset module
  local charset = setmetatable( {}, table ) do charset.__index = charset
  charset.match = dofile("set/match.lua")( dict.indexlist, normalize )
  charset.match2 = dofile("set/match2.lua")
  charset.ow    = dofile("set/overwrite.lua")
  charset.replace = dofile("set/replace.lua")
  charset.print = dofile("set/print.lua")( de_normalize )
  charset.wipe  = dofile("set/wipe.lua")
  charset.tprint = function(self)table.print(self,print,{})end
  charset.ismatch = dofile("set/ismatch.lua")
  charset.addrandom  = dofile("set/addrandom.lua")--| f(int, function) -> nil
  charset.replaceall = dofile("set/replaceall.lua")-- depends on :wipe :addrandom
  charset.getstring  = dofile("set/getstring.lua")
  charset.perm = dofile("set/permutation.lua")
  charset.removethis  = dofile("set/removethis.lua")
end

  -- board module
  local board  = dofile("board/board.lua")( number_of_rows_and_columns ) do-- singleton
  board.backup = dofile("board/writebackup.lua")( board )-- to declare .undo_grid
  board.tprint = dofile("board/tprint.lua")
  board.wipe   = dofile("board/wipe.lua")-- depends on :backup
  board.print  = dofile("board/print.lua")( clearconsole )
  board.undo   = dofile("board/undo.lua")
  board.ms     = dofile("board/make_suggestions.lua")( normalize, charset, dict.indexlist )
  board.h, board.v = dofile("board/fits.lua")()
  board.print_result_to_console = dofile("board/print_result_to_console.lua")( de_normalize )
  board.allConnectionsAreValid  = dofile("board/allConnectionsAreValid.lua")( dict.hashlist )
  --board.user_stops_browsing_results = dofile("board/user_stops_browsing_results.lua") end
  board.hinsert, board.vinsert  = dofile("board/insert.lua")()
end

  -- game module
  local game   = dofile("game/loadandsave.lua")( {}, board, charset )

  number_of_rows_and_columns, clearconsole, cleanupdictionary = nil, nil, nil

  -- return game board, character set, dictionary, and game object
  return board, charset, dict, game
end
