return function( game, board, charset )

  local defaultpath = "game/savedgame"

  function game.save( fileName )

    -- open file, check for error
    fileName = fileName or defaultpath
    local file = io.open( fileName, "w" )
    assert( file, "\n\n\tCould not open file: "
      .. tostring(fileName) .. "\n" )
  
    -- write board to file
    for r=1, #board do
      for c=1, #board do
        file:write( board[r][c] ..";" )
      end
      file:write("\n")
    end
    
    -- save current charset as well
    file:write( "\n" .. charset:getstring() .. "\n" ) 
    print( "done saving:", fileName )
    print( "file closed:", file:close() )
  end

  function game.load( fileName )
    board:backup()
    fileName = fileName or defaultpath
    local file = io.open( fileName, "r" )
    assert( file, "\n\n\tCould not open file: "
      .. tostring(fileName) .. "\n" )

    local line, r = file:read("l"), 1
    while line and line~="" do
      local c = 1-- match any char that is not a semicolon ;
      for cell in line:gmatch("[^;]") do
        board[r][c] = cell
        c = c + 1
      end
      line  = file:read("l")
      r = r + 1
    end

    board:print()-- at this point, it's expected that line==""
    print( "charset loaded:", pcall(charset.ow, charset, file:read("l")) )
    print( "done loading:", fileName )
    print( "file closed:", file:close() )
    charset:print()
  end

  return game
end