-- charset:ismatch()
-- nPattern = normalized board pattern, i.e. it consists of dots and alpha-numeric chars, e.g. ".a..r"
return function( self, nWord, nPattern, fset )--| (string, string, table) => boolean

  for m in nPattern:gmatch("%w+") do
    local a, b = nWord:find(m, fset[1].a)
    nWord = nWord:sub(1, a-1) .. nWord:sub(b+1, -1)
  end

  -- now, check which chars in charset match what's left over to match
  local alreadymatched, overlap = {}, ""
  for char1 in nWord:gmatch("%w") do
    for i, char2 in ipairs(self) do
      if not(alreadymatched[i]) and char1==char2 then
        alreadymatched[i] = true--keep track of what's already been matched
        overlap = overlap .. char1
        break-- move on to the next letter in word
      end
    end
  end

  -- copy overlap to fset
  fset.overlap = overlap
  return #nWord == #overlap--valid? true or false
end
