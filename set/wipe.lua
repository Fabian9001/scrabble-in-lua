--charset:wipe()
return function( self )--| f() -> int
  local num = #self
  for i=num, 1, -1 do self:remove(i) end
  return num
end
