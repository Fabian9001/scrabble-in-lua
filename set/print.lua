return function( de_normalize )
--charset:print()  
  return function(self)
    local str, indices = "", ""
    for k, v in ipairs(self) do
      if k < 11 then str = str .. " " .. v
      else str = str .. "  " .. v end
      indices = indices .. " " .. k
    end
    print( "charset:\n", de_normalize(str), "\n", indices )
  end
end