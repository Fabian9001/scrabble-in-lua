-- overwrite indexed chars with non-alpha-numerical chars from input string
-- boolean is true if length of array remains the same, else false
return function( self, nStr )--| f(str) -> nil
  local i = 0
  for char in nStr:gmatch("%w") do
    i = i + 1
    self[i] = char
  end
  return self:getstring()
end
