--charset:getstring()  
return function(self)--| f() -> str
  local s = ""
  for _, char in ipairs(self) do s=s..char end
  return s
end