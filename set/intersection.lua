--charset:intersec()
return function( self, nWord )--| f(str) -> str
  local word, overlap = {nWord:byte(1,-1)}, ""
  for _, char in ipairs(self) do
    for i, byte in ipairs(word) do
      if char:byte() == byte then
        word[i] = -1-- don't match more than once
        overlap = overlap .. char
        break
      end
    end
  end
  return overlap
end
