--charset:removethis()
return function( self, nStr )--| f(str) -> int

  local removed = ""

  for char1 in nStr:gmatch("%w") do
    for i, char2 in ipairs(self) do
      if char1 == char2 then
        removed = removed .. self:remove(i)
        break
      end
    end
  end

  return removed:len()
end