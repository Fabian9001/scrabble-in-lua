
return function( self, str, n, root, yield )

  local iter = coroutine.wrap( self.perm )
  local running, str = iter( str or self:getstring(), n )

  while running do
    if str and root:isword(str) then yield("yield:",str) end
    running, str = iter()
  end

  print( "", "n = ".. n, str:len() )
end
