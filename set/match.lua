return function( indexlist, normalize )

   -- tries finding matches to letters in player's set + some board pattern
  return function( self, nPattern, matchlimit--[[nil]], list--[[nil]] )
    list  = list or indexlist
    matchlimit = matchlimit or 49
    local count, str = 0, ""
    print("pattern = '" .. nPattern ..
      "'\tlimit #results = " .. matchlimit)
    --nPattern2 = nPattern:gsub("%[[^%]]+%]","."):gsub("[^%.%w]","")
    for i=1, #list do
      local page = list[i]
      local nWord = normalize(page.title)
      local fset = nWord:gfind( nPattern )
      if #fset > 0 and self:ismatch(nWord, nPattern, fset) then
        count = count + 1
        str = str .. " " .. page.title
        if count%7 == 0 then str = str .. "\n" end
        if count == matchlimit then break end
      end
    end
    return str
  end
end
-- do local _, len = nPattern:gsub("%[[^%]]+%]","."):gsub("%w","|") -- use :gsub to count alpha-numeric chars in pattern (and not stuff like ^ + . $ ? ] [)
-- print("naked pattern: ".. _, "maxWordLength = ".. 8+len) end
