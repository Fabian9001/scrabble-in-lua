local getweightedstring = function ()--| f() -> str
  -- set up characters and their weights
  local keys = { 'a','b','4','3','2','1','i','j','g','h','e','f',
    'c','d','q','r','o','p','m','n','k',
    'l','y','z','w','x','u','v','s','t' }
  local weights={
    ["e"]   =   13.3110243241029,
    ["n"]   =   8.15213287729991,
    ["r"]   =   8.08803329965027,
    ["i"]   =   7.26967929326601,
    ["t"]   =   6.81230711890873,
    ["s"]   =   6.6579219206097,
    ["a"]   =   6.52514422404971,
    ["l"]   =   5.05639638279175,
    ["h"]   =   4.55629115666804,
    ["u"]   =   3.96051851578151,
    ["o"]   =   3.49776454731079,
    ["g"]   =   3.38956638804503,
    ["c"]   =   3.13180254759675,
    ["m"]   =   2.62197796195473,
    ["k"]   =   2.546472193796,
    ["b"]   =   2.43859533567133,
    ["f"]   =   2.22934796752931,
    ["d"]   =   2.2186647045877,
    ["p"]   =   2.00845343302237,
    ["z"]   =   1.25837591912208,
    ["w"]   =   1.06704108959643,
    ["v"]   =   0.790240156537916,
    ["1"]   =   0.720758784774824,
    ---------- 1.6924116367591
    --[[ 1× ]] ["3"]   =   0.590872798484744,
    --[[ 1× ]] ["y"]   =   0.286761268432645,
    --[[ 1× ]] ["2"]   =   0.262021080567868,
    --[[ 1× ]] ["4"]   =   0.174386194332569,
    --[[ 1× ]] ["j"]   =   0.157196583283666,
    --[[ 1× ]] ["x"]   =   0.145228115777653,
    --[[ 1× ]] ["q"]   =   0.0750238164470841
  }
  -- create "weighted" string of characters
  local str = ""; for _, char in pairs(keys) do
    str = str .. char:rep( math.ceil(weights[char]*1.6924116367591) ) 
  end
  return str
end

--charset:addrandom
return function( self, numChars )--| f(int, function) -> nil
  local str, tmp = getweightedstring(), ""

  for i=1, numChars do
    local rando = math.random( 1, str:len() )-- pick randomly
    local char = str:sub(rando,rando)
    self:insert( char )
    tmp = tmp .. char
  end

  return tmp
end
