
return function( source, yield )

  local function f( n, s, j )
    local i = 0
    for char in source:gmatch(".", j) do
      n, s, i = n-1, s..char, i+1
      if n == 0 then
        yield( s )
      else
        f( n, s, i+j )
      end
    end
  end

  return f
end


-- do
--   local _, s, j, i = foo( 2, "", 1, 0 )
--   print( _, s, j, i )
--   _, s, j, i = foo( 2, "", 1, i )
--   print( _, s, j, i )
-- end