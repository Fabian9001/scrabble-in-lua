-- charset.perm = dofile("set/permutation.lua")

-- subset, "", results, yield
local function perm( str, prmt, results, yield ) --> string|nil, boolean
  if #str == 0 then
    if not(results[prmt]) then
      results[prmt] = true
      yield( prmt )
    end--else print( "redundand perm:", prmt, original ) 
    return
  end
  for c in str:gmatch(".") do
    local a, b = str:find( c )
    perm(str:sub(1,a-1) .. str:sub(b+1,-1), prmt..c, results, yield)
  end
  return nil
end

-- source, n, "", setmetatable({},table), yield
local function get_all_subsets( source, n, subset, results, yield ) --> string|nil, table

  if n == 0 then
    if not(results[subset]) then
      yield(subset, results)
    end
    return
  end

  local i = 0
  for char in source:gmatch(".") do
    i = i + 1
    get_all_subsets(source:sub(i+1,-1), n-1, subset..char, results, yield)
  end

  return nil, results
end

return function( source, n, yield1, yield2, yield3 )

  yield1 = yield1 or coroutine.yield
  yield2 = yield2 or yield1
  yield3 = yield3 or yield2

  -- subset loop
  local get_all_subsets_wrap = coroutine.wrap( get_all_subsets )
  local subset, results = get_all_subsets_wrap( source, n, "", setmetatable({},table), yield3 )
  while subset do

    -- permutation loop
    local perm_wrap = coroutine.wrap( perm )
    local prmt = perm_wrap( subset, "", results, yield2 )
    while prmt do
      yield1( true, prmt )
      prmt = perm_wrap()-- perm_wrap yields next permutation
    end

    -- get next subset
    subset, results = get_all_subsets_wrap()
  end

  -- return all permutations of all subsets of size n
  return false, results
end

--[[
coroutine.close (co)
Closes coroutine co, that is, closes all its pending to-be-closed variables
and puts the coroutine in a dead state.
The given coroutine must be dead or suspended.
In case of error
(either the original error that stopped the coroutine or errors in closing methods),
returns false plus the error object; otherwise returns true.

coroutine.create (f)
Creates a new coroutine, with body f.
f must be a function.
Returns this new coroutine, an object with type "thread".

coroutine.isyieldable ([co])
Returns true when the coroutine co can yield. The default for co is the running coroutine.
A coroutine is yieldable if it is not the main thread and it is not inside a non-yieldable C function.

coroutine.resume (co [, val1, ···])
Starts or continues the execution of coroutine co.
The first time you resume a coroutine, it starts running its body.
The values val1, ... are passed as the arguments to the body function.
If the coroutine has yielded, resume restarts it;
the values val1, ... are passed as the results from the yield.
If the coroutine runs without any errors,
resume returns true plus any values passed to yield (when the coroutine yields)
or any values returned by the body function (when the coroutine terminates).
If there is any error, resume returns false plus the error message.

coroutine.running ()
Returns the running coroutine plus a boolean, true when the running coroutine is the main one.

coroutine.status (co)
Returns the status of the coroutine co, as a string: "running", if the coroutine is running (that is, it is the one that called status); "suspended", if the coroutine is suspended in a call to yield, or if it has not started running yet; "normal" if the coroutine is active but not running (that is, it has resumed another coroutine); and "dead" if the coroutine has finished its body function, or if it has stopped with an error.

coroutine.wrap (f)
Creates a new coroutine, with body f; f must be a function.
Returns a function that resumes the coroutine each time it is called.
Any arguments passed to this function behave as the extra arguments to resume.
The function returns the same values returned by resume, except the first boolean.
In case of error, the function closes the coroutine and propagates the error.

coroutine.yield (···)
Suspends the execution of the calling coroutine.
Any arguments to yield are passed as extra results to resume. 
]]