--charset:gettaggedstring()
return function( self )--| f() -> str
  local str = ""
  for _, char in ipairs(self.taggedchar) do
    str = str .."'".. char .. "' "
  end
  return str:sub(1,-2)
end