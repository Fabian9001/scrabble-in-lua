
return function( str, pattern )

  local list, a, b = setmetatable({}, table), 0, 0-- metatable for debugging

  -- main loop
  while a do
    a, b = str:find( pattern, a+1 )-- _G.string.find
    list:insert( setmetatable({a=a,b=b}, table) )-- metatable for debugging
  end
  list:remove( #list )--remove very last element which is always empty
  --list.pattern = pattern--for debugging
  --list.string = str--for debugging

  return list
end
