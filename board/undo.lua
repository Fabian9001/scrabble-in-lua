return function(self) -- takes the game 1 step back. Calling this function twice in direct succession will undo the recent undo
  local redo = {} -- tmp
  for row=1, #self.undo_grid do
    redo[row] = {}
    for column=1, #self.undo_grid do -- current grid and undo_grid swap places
      redo[row][column] = self[row][column]
      self[row][column] = self.undo_grid[row][column]
      self.undo_grid[row][column] = redo[row][column]
    end
  end
end