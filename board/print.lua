
return function( clearstring )

  return function(self)

    local getReadableChar = setmetatable(
      { -- to make for readable words, normalized special chars (ä,ö,ü,ß) need to be transformed back. All chars are transformed to uppercase.
        ['a']='A',
        ['b']='B',
        ['c']='C',
        ['d']='D',
        ['e']='E',
        ['f']='F',
        ['g']='G',
        ['h']='H',
        ['i']='I',
        ['j']='J',
        ['k']='K',
        ['l']='L',
        ['m']='M',
        ['n']='N',
        ['o']='O',
        ['p']='P',
        ['q']='Q',
        ['r']='R',
        ['s']='S',
        ['t']='T',
        ['u']='U',
        ['v']='V',
        ['w']='W',
        ['x']='X',
        ['y']='Y',
        ['z']='Z',
        ---------
        ['1']='Ä',
        ['2']='Ö',
        ['3']='Ü',
        ['4']='ẞ'
      },-- any other char remains unchanged:
      {__index=function(tbl,key) return key end}
    )

    -- clear console screen
    os.execute( clearstring )
    
    -- draw all rows except the very last one
    print("")
    local str, gridCenter = "", math.ceil(#self/2)
    for row=1, #self do
      str = ""
      for column=1, #self do
        if row~=gridCenter or column~=gridCenter then
          str = str .."  ".. getReadableChar[self[row][column]]
        elseif self[row][column]=='-' then
          str = str .."  @"
        else
          str = str .."  ".. getReadableChar[self[row][column]]
        end
      end
      print(str .." ".. row) -- last column displays indices of rows
    end
      
    -- very last row displays index of each column: 1 2 3 4 ....
    str = ""
    for column=1, #self do
      if column < 11 then
        str = str .."  ".. column
      else-- For all columns to align neatly, ...
        str = str .." ".. column-- ... we need to adjust the space between double digit indices.
      end
    end
    
    -- print last string
    print(str)
  end
end
