
-- these functions check if:
  -- a word fits board dimensions
  -- all strings on the board (resulting from the insertion) amount to actual words
  -- user wants to accept suggestion made
-- note: mutiple coordinates result from all the ways a word fits the pattern:
-- e.g., think of pattern "te" in word "etepetete" where aSet={2, 6, 8}

return function()

-- helper function
  local function user_stops_browsing_results( self )
    -- insertion is valid, now, let user decide how to proceed
    local user=io.read()
    if user=="exit" then -- user rejects suggestion and quits browsing
      self:undo()
      return true
    elseif user=="ok" then -- user accepts suggestion and quits browsing
      return true
    else-- Here, user wants to keep browsing results.
      self:undo()-- Undo the most recent insertion.
      self:print()
      print("\n\tsearching ...")
      return false
    end
  end

-- aka board.h f(board, int, int, charset, table, str, str) -> boolean
  return function( self, charset, fset, nWord, boardsnip )
      local pattern, r, c = table.unpack(boardsnip)
      -- there are #fset different ways to match word to board pattern
      for k=1, #fset do
        -- calculate word coordinates
        local cStart = c - fset[k].a + 1-- column in which word starts
        local cEnd = c - fset[k].a + nWord:len()-- column in which word ends
        local connections = {}-- for printing results to console
        if self:hinsert(nWord, r, cStart)-- attempt to insert word
          and self:allConnectionsAreValid(r, cStart, r, cEnd, connections) then--check if all (newly) connected strings amount to actual words --> match
            self:print_result_to_console(connections, charset, pattern, fset.entrymatch, fset.overlap)
            if user_stops_browsing_results(self) then return true end -- get user input
        end
      end
      return false
    end,

-- aka board.v f(board, int, int, charset, table, str, str) -> boolean
    function( self, charset, fset, nWord, boardsnip )
      local pattern, r, c = table.unpack(boardsnip)
      -- there are #fset different ways to match word to board pattern
      for k=1, #fset do
        -- calculate coordinates of word to be inserted below
        local rStart = r - fset[k].a + 1-- row in which word starts
        local rEnd = r - fset[k].a + nWord:len()-- row in which word ends
        local connections = {}-- for printing results to console
        if self:vinsert(nWord, rStart, c)-- attempt to insert word
          and self:allConnectionsAreValid(rStart, c, rEnd, c, connections) then--then, check if all (newly) connected strings amount to actual words --> match
            self:print_result_to_console(connections, charset, pattern, fset.entrymatch, fset.overlap)
            if user_stops_browsing_results(self) then return true end-- get user input
        end
      end
      -- either, word can't be inserted (for one reason or another), or, user rejected all matches
      return false
    end

end


