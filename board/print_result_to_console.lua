return function( de_normalize )
  return function( self, connections, charset, pattern, entry, overlap )
    self:print() -- refresh screen
    for title, entry in pairs(connections) do print("", title, entry.bedeutung[1]) end
    print( "\n\t\t¡match!\n",
      "\nFrom ".. de_normalize(charset:getstring()) .." we take {"..
      overlap ..
      "} and put it on the board where '".. de_normalize(pattern) ..
      "' to form the word '".. entry.title .."':",
      "\n\t1):", entry.bedeutung[1],
      "\n\t2):", entry.bedeutung[2],
      "\n\tWortart:", entry.wortart,
      "\n\n\tPress enter to skip to next suggestion."..
      "\n\tEnter 'ok' to accept suggestion." ..
      "\n\tEnter 'exit' to reject suggestion and stop browsing results." )
      --.."\n\tEnter 'remove [word]' to remove word from dictionary" )
  end
end