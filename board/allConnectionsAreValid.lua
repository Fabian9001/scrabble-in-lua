return function( hashlist )
  return function( self, rStart, cStart, rEnd, cEnd, connections )--| f(int, int, int, int, table) -> boolean
    
    local function horizontalConnectionIsValid( r, c )

      local function getFullHorizontalString( self, r, c )
        local str = "" -- Abort if inital cell [r|c] is empty or out of bounds ...
        if self[r][c]:match("[%-#]") then return str end 
        -- ... else go to the left until we hit an empty cell (-) or a cell out of bounds (#)
        repeat c = c - 1 until self[r][c]:match("[%-#]") 
        c = c + 1 -- farthest non-empty cell to the left
        while self[r][c]:match("[^%-#]") do -- While cell is not empty or out of bounds ... 
          str = str .. self[r][c] -- ... add cell to string and ...
          c = c + 1 -- ... move 1 cell to the right.
        end
        return str
      end
    
      -- check if connecting cell is not empty and within bounds
      local dummy = {title="-",bedeutung={"-"}}
      if self[r][c-1]:match("[^%-#]") or self[r][c+1]:match("[^%-#]") then 
        -- cell is within bounds & non-empty, so we must check if string amounts to an actual word
        local word = hashlist[getFullHorizontalString(self,r,c)] or dummy--print( "r=".. r ..", c=".. c .." (horizontal)", word.title, word.bedeutung[1] )
        return word.title~="-", word -- newly connected word is valid, if word.title=="-" word is NOT valid
      else -- connecting cell either is empty or outside bounds, in any case, no further checks needed
        return true, dummy -- no connection --> valid
      end
    end
    
    local function verticalConnectionIsValid( r, c )

      local function getFullVerticalString( self, r, c )
        local str = "" -- inital cell [r|c] should not be empty
        if self[r][c]:match("[%-#]") then return str end 
        -- go upwards until we hit empty cell ('#' is any cell out of bounds, '-' is any empty cell within bounds)
        repeat r = r - 1 until self[r][c]:match("[%-#]") 
        r = r + 1 -- non-empty cell in the furthest upward direction
        while self[r][c]:match("[^%-#]") do -- while cell is not empty or out of bounds do ... 
          str = str .. self[r][c]
          r = r + 1
        end
        return str
      end
    
      -- check if connecting cell is not empty and within bounds
      local dummy = {title="-",bedeutung={"-"}}
      if self[r-1][c]:match("[^#%-]") or self[r+1][c]:match("[^#%-]") then
        -- check if string amounts to an actual word:
        local word = hashlist[getFullVerticalString(self,r,c)] or dummy--print( "r=".. r ..", c=".. c .." (vertical)", word.title, word.bedeutung[1] )
        return word.title~="-", word -- newly connected word is valid, if word.title=="-" is false
      else -- connecting cell is empty or outside bounds, so no further checks needed
        return true, dummy -- no connection --> valid
      end
    end
  
    local currentR = rStart-1
    local isvalid  = true
    local currentC = cStart-1
    local result   = {title="-",bedeutung={"-"}}
  
    while isvalid == true and currentR < rEnd do
      connections[result.title] = result
      currentR = currentR+1 -- moving "down" 1 row (as perceived by looking on the screen)
      isvalid, result = horizontalConnectionIsValid(currentR, cStart) -- looking for a connection, if there is one, is it valid?
    end
  
    while isvalid == true and currentC < cEnd do
      connections[result.title]=result
      currentC = currentC+1 -- moving to the right 1 column
      isvalid, result = verticalConnectionIsValid(rStart, currentC) -- looking for a connection, if there is one, is it valid?
    end
  
    -- clean up the failed insertion attempt, if necessary
    if isvalid==false then self:undo() end
    
    --remove dummy and return boolean
    connections['-'] = nil
    return isvalid
  end
end