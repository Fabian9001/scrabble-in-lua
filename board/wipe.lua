return function(self)

  self:backup()

  -- overwrite all cells with a minus sign '-'
  for row=1, #self do
    for column=1, #self do
      self[row][column] = '-'
    end
  end
end