return function()

  return

-- hinsert:
    function( self, nWord, row, column )
      self:backup()-- enable clean up
      local cc = 0
      for x in nWord:gmatch(".") do
        if self[row][column+cc]:match("[".. x .."%-]") then-- check if cell is valid (i.e. empty and inside grid)
          self[row][column+cc] = x
        else
          self:undo()-- clean up failed insertion attempt
          return false
        end
        cc = cc + 1
      end
      return true
    end,

-- vinsert:
    function( self, nWord, row, column )
      self:backup() -- enable clean up
      local rr = 0
      for x in nWord:gmatch(".") do
        if self[row+rr][column]:match("[".. x .."%-]") then
          self[row+rr][column] = x
        else
          self:undo()-- clean up failed insertion attempt
          return false
        end
        rr = rr + 1
      end
      return true
    end

end
