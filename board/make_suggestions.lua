
return function( normalize, charset, indexlist )

   -- for example, B:ms( boardsnip={".", 7, 8}, is_valid_insertion=B.h) -- for suggesting words matching the empty cell at row 7, column 8, placed horizontally
  return function( self, boardsnip, is_valid_insertion )

    -- clear screen and print grid, set up board pattern for words to match
    self:print()
    local nPattern = normalize( boardsnip[1] )

    -- iterate through whole dictionary
    for i=1, #indexlist do
    
      -- normalize word, then try fitting word to pattern
      local nWord = normalize( indexlist[i].title )
      local fset = nWord:gfind(nPattern) -- find out in how many ways word matches pattern
      fset.entrymatch = indexlist[i]

      -- if #fset == 0 then word and pattern on the board don't match
      if #fset > 0-- check if charset + pattern will cover entire word:
        and charset:ismatch(nWord, nPattern, fset) -- when it's a match, check if 1) word fits board dimensions, 2) all strings on the board (resulting from the insertion) amount to actual words, 3) user wants to accept suggestion made
          and is_valid_insertion(self, charset, fset, nWord, boardsnip) then
            charset:replace( fset.overlap )-- insertion successfull and accepted
            break
      end
    end

    self:print()
  end
end
