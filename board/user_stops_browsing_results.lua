-- board:user_stops_browsing_results
return function( self, charset, i )
  -- insertion is valid, now, let user decide how to proceed
  local user=io.read()
  if user=="exit" then -- user rejects suggestion and quits browsing
    self:undo()
    return true
  elseif user=="ok" then -- user accepts suggestion and quits browsing
    charset:fill( charset:removetagged(), table.insert )-- replace used chars
    return true
  else-- Here, user wants to keep browsing results.
    self:undo()-- Undo the most recent insertion.
    self:print()
    print("\n\tsearching ...")
    return false
  end
end
