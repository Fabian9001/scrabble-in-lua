
-- sets up game board (usual dimensions: 15x15 )
return function( number_of_rows_and_columns )

  local board = {} -- board[row][column], e.g. board[3][7]

  -- we might try to access a cell that is out of bounds in its r- or c-position 
  local column_out_of_bounds = function()return'#'end
  local row_out_of_bounds = function()return setmetatable({},{__index=column_out_of_bounds}) end
  setmetatable( board, {__index=row_out_of_bounds} )
  for row=1, number_of_rows_and_columns do
    board[row] = setmetatable( {}, {__index=column_out_of_bounds} )
    for column=1, number_of_rows_and_columns do board[row][column]='-' end
  end
  
  number_of_rows_and_columns=nil
  return board
end