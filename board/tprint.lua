local print = _G.print
return function( self )
   table.print(
      self,
      print,
      {
         [self.undo_grid] = true,
         [self[1]] = true,
         [self[2]] = true,
         [self[3]] = true,
         [self[4]] = true,
         [self[5]] = true,
         [self[6]] = true,
         [self[7]] = true,
         [self[8]] = true,
         [self[9]] = true,
         [self[10]] = true,
         [self[11]] = true,
         [self[12]] = true,
         [self[13]] = true,
         [self[14]] = true,
         [self[15]] = true
      }
   )
end
