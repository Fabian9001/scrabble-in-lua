
return function( self )

  self.undo_grid = {}

  return function(self)-- needs to be called by every function that intents to modify the board (call needs to happen before modification starts)
    self.undo_grid = {}
    for row=1, #self do
      self.undo_grid[row] = {}
      for column=1, #self do
        self.undo_grid[row][column] = self[row][column]
      end
    end
  end
end


